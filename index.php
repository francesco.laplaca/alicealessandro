<?php

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Alice e Alessandro 2024">
    <meta property="og:image" content="" />
    <meta property="og:title" content="Alice e Alessandro 2024" />
    <meta property="og:description" content="Alice e Alessandro 2024" />
    <meta property="og:site_name" content="Alice e Alessandro 2024" />
    <meta property="og:type" content="website" />

    <title>Alice e Alessandro 2024</title>

    <!-- icona sito -->
    <link rel="icon" type="image/png" href="img/linea.png">

    <!-- Bootstrap -->
    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css">
    <!-- FancyBox -->
    <link rel="stylesheet" type="text/css" href="js/fancybox/source/jquery.fancybox.css?v=2.1.5" media="screen" />

    <!-- LightBox -->
    <link href="./js/lightbox/css/lightbox.css" rel="stylesheet">

    <!--CSS include-->
    <link href="css/custom.css?v=5" rel="stylesheet">

    <!-- CSS fonts -->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:300,400' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Arvo:400,700' rel='stylesheet' type='text/css'>
</head>


<body onresize="" onload="">

    <!-- Navbar
================================================== -->
    <nav class="navbar navbar-inverse navbar-fixed-top">
        <div class="nav-back">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                </div>
                <div id="navbar" class="navbar-collapse collapse">

                    <ul class="nav navbar-nav navbar-center">
                        <li><a class="nav-link" href="#bio-cerimonia">Cerimonia</a></li>
                        <li><a class="nav-link" href="#bio-ricevimento">Ricevimento</a></li>
                        <li><a class="nav-link" href="#bio-menu">Menù</a></li>
                        <li><a class="nav-link" href="#bio-lista">Lista nozze</a></li>
                        <li><a class="nav-link" href="#bio-conferma">RSVP</a></li>
                        <li><a class="nav-link" href="#bio-notizie">Info</a></li>
                    </ul>

                </div>
            </div>
        </div>
    </nav>
    <!-- /.navbar -->


    <!-- HOME
================================================== -->
    <div id="bio-top" class="container spazio">
        <div class="inner">
            <div class="copy">
                <h1>Alice e Alessandro</h1>
                <p>Domenica 23 giugno 2024</p>
            </div>
        </div>
    </div>

    <!-- partecipazione
================================================== -->
    <div class="container spazio" id="bio-partecipazione">
        <div class="inner-accentra centra">
            <div class="accentra">
                <p class="lead">
                    Cari tutti,
                    <br>siamo molto felici di invitarvi al nostro matrimonio.
                    <br>Abbiamo deciso di compiere questo importante passo, di sceglierci per tutta la vita.
                    <br><br>
                    Il 23 giugno sarà l'inizio di un nuovo cammino insieme, speriamo costellato di molte gioie e pochi dolori, molti momenti belli e pochi attimi di stanchezza.
                    <br><br>
                    Per vivere la nostra favola, ci piacerebbe avervi tutti con noi il 23 giugno, ma, anche dopo, rimaneteci vicini: solo con voi tutti il nostro sarà un "per sempre felici e contenti".
                    <!-- <br><br>
                    <span class="text-muted">Simone chiamato Pietro</span><br>
                    M. Giuseppe Lepori -->
                </p>
            </div>
        </div>
    </div>


    <!-- cerimonia
================================================== -->
    <div class="container spazio" id="bio-cerimonia">

        <div class="row featurette" style="">
            <div class="col-md-7">
                <h2 class="featurette-heading">Cerimonia <span class="text-muted"></span></h2>
                <br>
                <p class="lead">
                    Ci sposeremo con rito cattolico.
                    <br>La cerimonia si terrà nel
                    <br><span class="text-muted"><a href="https://it.wikipedia.org/wiki/Santuario_di_Nostra_Signora_di_Roverano" target="_blank">Santuario di Nostra Signora di Roverano</a></span>,
                    <br>situato a Termine di Roverano, frazione di Borghetto di Vara,
                    <br>paese di origine della famiglia Dinegro
                    <br>(e di adozione di Alessandro).

                </p>

                <a href="https://www.google.com/maps/place/Santuario+N.S.+di+Roverano/@44.2344991,9.6666519,17z/data=!3m1!4b1!4m6!3m5!1s0x12d4e918743ef9b1:0xff680dcd1ac4e141!8m2!3d44.2344991!4d9.6692268!16s%2Fg%2F122h6zqk?hl=it&entry=ttu" target="_blank">Link Google Maps</a>
            </div>
            <div class="col-md-5">
                <br>
                <br>
                <br>
                <img class="featurette-image img-responsive center-block max-foto2" src="img/chiesa2.jpg" alt="Chiesa">
            </div>
        </div>

    </div>


    <!-- ricevimento
================================================== -->
    <div class="container spazio" align="center" id="bio-ricevimento" style="">

        <div class="row featurette">
            <div class="col-md-7 col-md-push-5">
                <h2 class="featurette-heading">Ricevimento <span class="text-muted"></span></h2>
                <p class="lead">A seguito della cerimonia, non vediamo l'ora di festeggiare
                    questo importante "inizio" con le persone a cui teniamo di più.
                    <br><br>
                    Vi invitiamo, quindi, al <a target="_blank" href="https://grandhoteltorrefara.com/">Grand Hotel Torre Fara</a> per una serata di festa,
                    gioia, relax, amore, che riempie il cuore, cibo,
                    che riempie lo stomaco, e vino, che riempie di allegria.

                    <br><a href="https://www.google.com/maps/place/Grand+Hotel+Torre+Fara/@44.3205285,9.3076593,17z/data=!3m1!4b1!4m9!3m8!1s0x12d4994205c533a3:0x415cdbca25984184!5m2!4m1!1i2!8m2!3d44.3205285!4d9.3102342!16s%2Fg%2F11nm9t4dr1?hl=it&entry=ttu" target="_blank"><span style="font-size: 12px;">Link Google Maps</span></a>
                    <br>
                    <br>
                </p>
            </div>
            <div class="col-md-5 col-md-pull-7">
                <br>
                <br>
                <br>
                <img class="featurette-image img-responsive center-block max-foto2" src="img/hotel.jpg" alt="Generic placeholder image">
            </div>
        </div>
    </div>

    <div class="container spazio" id="bio-indicazioni">
        <div class="inner-accentra centra">
            <div class="accentra">
                <!-- <h2>Indicazioni</h2> -->
                <p class="lead">
                    Il Grand Hotel Torre Fara si trova a Chiavari, a una mezz'ora circa dal luogo della cerimonia.
                    Dal Santuario di Roverano prenderemo l'autostrada a Carrodano/Levanto e usciremo a Chiavari.
                    <br><br>Dall'uscita dell'autostrada, in meno di un chilometro ci troveremo sul lungomare: bisognerà girare a destra e costreggiarlo fino alla fine, dove si trova l'hotel.
                    <br><br>Abbiamo qui riservato tutto il parcheggio in superficie: vi basterà suonare alla sbarra e dichiarare: "Siamo qui per festeggiare con Alice e Alessandro, la coppia più bella del mondo!" o, più semplicemente "ospiti matrimonio Alice e Alessandro"
                    <br>(noi preferiamo la prima opzione).
                </p>
            </div>
        </div>
    </div>

    <div class="container spazio" id="bio-menu">
        <div class="inner-accentra centra">
            <div class="accentra">
                <h1 class="featurette-heading" style="margin-top: 0;">Menù <span class="text-muted"></span></h2>
                    <p class="lead">
                        Il menù è principalmente di pesce, con qualche pietanza di terra e assaggi della tradizione ligure.
                        <br><br>Chi avesse esigenze particolari, allergie, intolleranze, preferenze culinarie, non esiti a farcelo sapere, nulla sarà un problema. Per i bimbi, ci sarà un menu apposito.
                    </p>
            </div>
        </div>
    </div>

    <!-- lista nozze
================================================== -->
    <div id="bio-lista" class="container spazio" align="center">

        <h1>Lista <span class="text-muted">nozze</span></h1>


        <p class="lead">
            Come sapete, qualche mese fa abbiamo comprato una casetta, a Torino, dove iniziare la nostra nuova vita insieme.
            <br><br>
            Nei mesi scorsi abbiamo lavorato per rendere la nostra casa completa ed accogliente e, finalmente, il grosso è stato scelto, comprato, montato/appeso/sistemato.
            <br>Qualunque pensiero sarà comunque molto apprezzato, ma per chi volesse indicazioni, lasciamo la nostra
            <br>"Lista viaggio di nozze"

            <br>

            <li>Volo aereo</li>
            <li>Hotel</li>
            <li>Pranzi e cene</li>
            <li>Ingresso musei e templi</li>
            <li>Spostamenti interni (treni, taxi e biciclette)</li>
            <li>Esperienze adrenaliniche</li>
            <li>Giornate relax</li>
            <li>Cenette gourmet</li>
            <li>Specialità gastronomiche local</li>
            <li>Souvenir, per noi e per voi ❤️</li>

            <br>

            IBAN: <span class="text-muted">IT52S0623001001000041964966</span>
            <br> Intestato a: Alice Dinegro e Alessandro Girardi
        </p>

        <p class="lead" style="margin-top: 50px;">
            Next stop:
            <br><br>
            <img class="featurette-image img-responsive center-block max-foto" src="img/osaka.jpg" alt="Japan">
        </p>

    </div>


    <!-- contatti e conferma
================================================== -->
    <div class="container spazio" align="center" id="bio-conferma">
        <div class="inner-accentra centra">
            <div class="accentra">
                <p class="lead">
                <h2>Contatti e <span class="text-muted">conferma</span></h2>
                <br>
                Speriamo davvero che possiate partecipare al nostro matrimonio;
                <br>vi chiediamo gentile conferma della vostra presenza.
                <br>Potrete contattarci ai nostri numeri di telefono
                <br>
                <br>
                Alice <span class="text-muted"><a href="tel:+393314070453">331 4070453</a></span>
                <br>
                Alessandro <span class="text-muted"><a href="tel:+393498094280">349 8094280</a></span> <br>

            </div>
        </div>
    </div>

    <!-- Notizie
================================================== -->
    <div class="container spazio" align="center" id="bio-notizie">
        <div class="row featurette" style="">
            <h2>Qualche notizia in più</h2>
            <div class="col-md-6">
                <h3 class="text-muted">I nostri testimoni</h3>
                <p class="lead">
                <h4 class="text-muted" style="color: #c19c6a;">Sofia</h4> Sofia Dinegro, sorella minore della sposa, 25 anni ancora da compiere. Sorella della sposa, ma a vederci, non si direbbe. Bassina, minuta, bionda e occhi azzurri la sposa. Alta, slanciata, castana e occhi nocciola la testimone. Però confermiamo, siamo sorelle. E chi meglio di mia sorella avrebbe potuto essere testimone del mio sì. L'unica con cui condividere questa emozione, e a cui delegare parte dell'organizzazione di questo matrimonio, nonostante le ansie, i ritardi, gli incastri di tempo, di giorni, di prospettive. Grazie di tutto, Sofi!
                </p>
                <p class="lead">
                <h4 class="text-muted" style="color: #c19c6a;">Elia</h4> "Elia Pescio, primo cugino/quasi fratello della sposa, 32 anni a Pasqua, neopapà, interista fedelissimo, audi lover, sillavenghese doc.
                Se mia sorella c'è da quando ho 5 anni, mio cugino è una presenza certa, costante, sicura nella mia vita da prima che nascessi. E lui sì, che mi assomiglia: biondo e occhi azzurri, da piccoli nessuno avrebbe detto che non eravamo fratelli. E come fratelli siamo cresciuti. Poi è arrivata la Sofi a completare il trio, e se penso a chi mi sarà sempre vicino, penso a loro. Il mio testimone non l'ho scelto adesso, è la vita che me l'ha messo vicino. Chi sono io per dire no? Grazie per esserci, Eli!"
                </p>
                <p class="lead">
                <h4 class="text-muted" style="color: #c19c6a;">Anna</h4> Anna Girardi, sorella maggiore dello sposo, sempre giovane dentro e fuori, mamma di due bimbi bellissimi e sposa di un simpatico burlone. Nonostante la differenza d'età prima e la lontananza poi, è sempre stata una figura di riferimento per me, con i suoi consigli e il suo esempio. Una testimone perfetta
                </p>
                <p class="lead">
                <h4 class="text-muted" style="color: #c19c6a;">Michele</h4> Michele Settembrino, testimone dello sposo con due mani sinistre. Esule anche lui dall'ovile trentino, insieme condividiamo molte passioni: sport e motagna solo per citarne qualcuna. Sempre disponibile, non si è mai tirato indietro quando ho avuto bisogno di una mano. Su di lui posso sempre contare!
                </p>
            </div>
            <div class="col-md-6" id="col-paggetti">
                <h3 class="text-muted">I nostri paggetti</h3>
                <p class="lead">
                <h4 class="text-muted" style="color: #c19c6a;">Caterina</h4> La nostra Cate è un amore di bambina, non si batte: è la nostra nipotina più grande, una bimba simpatica, gentile, educata, intelligente, fantasiosa, sognatrice e tanto tanto carina. L'unica a cui la sposa ha concesso una deroga speciale: potrà vestirsi di bianco, se lo vorrà. Porterà, insieme a Pietro, il simbolo del nostro matrimonio. Attendiamo il loro ingresso in Chiesa con le fedi!
                </p>
                <p class="lead">
                <h4 class="text-muted" style="color: #c19c6a;">Pietro</h4> Pietro, o Pietrino, come ci piace chiamarlo, è il nostro nipotino maschio più grande. Vivace e dolcissimo, non preoccupatevi se davanti al suo sorriso e ai suoi occhioni vi sciogliete: a noi succede sempre. Con Caterina, potrerà le fedi fino al banchetto degli sposi: non vediamo l'ora di vederlo concentrato ed elegante mentre percorre la navata!
                </p>
                <p class="lead">
                <h4 class="text-muted" style="color: #c19c6a;">Emma</h4> La bimba dai lunghi riccioli biondi, che sembra uscita da una favola, è la piccola Emma, la seconda nipotina dei ramo Bergamaschi della famiglia. Se se la sentirà, potrà portare anche lei le nostre fedi in chiesa, insieme a Caterina e Pietro. Che magico trio!
                </p>
                <p class="lead">
                <h4 class="text-muted" style="color: #c19c6a;">Enea</h4> Enea, l'ultimo arrivato di casa Pescio (ma anche Venturi e Dinegro) è il più piccolo concentrato di amore sconfinato: un fagottino di sorrisi e dormite che non smetteremmo mai di guardare e coccolare. è la nostra mascotte, il nostro piccolo principe troiano, e come tale avrà un posto d'onore al matrimonio e nei nostri cuori!
                </p>
            </div>
        </div>
    </div>

    <!-- Band
================================================== -->
    <div class="container spazio" align="center" id="bio-band">
        <div class="inner-accentra centra">
            <div class="accentra">
                <p class="lead">
                <h2>Accompagnamento <span class="text-muted">musicale</span></h2>
                <br>
                Ad allietare il nostro ricevimento ci sarà una band d'eccezione, che farà tappa proprio a Chiavari lungo il tour europeo.
                <br>Loro sono i The Crooked (nome che lascia intenedere la loro sobrietà ed eleganza), anche soprannominati "Crocchette" o "Crucchi".
                <br><br>
                <a href="https://www.instagram.com/the_crooked_?igsh=MTEweGZnMnhtbXZ5ZA==" target="_blank" style="vertical-align: middle; line-height: 20px;"><img style="width: 20px; height: 20px;" src="img/ig.png" /> @the_crooked_</a>


            </div>
        </div>
    </div>

    <!-- Footer
================================================== -->
    <div class="blog-footer">
        <footer>

            <!--<p class="pull-right">Pietro <span style="color: #585858;">e</span> Giulia  &middot; <a href="#"></a> &middot; <a href="#"></a> &middot; <span style="color: #585858;">2016</span></p>-->

        </footer>
    </div>
    <!-- /.footer -->

    <div id="back-to-top-btn">
        <img src="img/arr.png">
    </div>


    <!-- JavaScript
================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="js/jquery/jquery.min.js"></script>
    <script src="js/jquery/jquery.easing.min.js"></script>

    <script src="bootstrap/js/bootstrap.min.js"></script>
    <script src="js/bootstrap.file-input.js"></script>
    <script src="js/holder/holder.js"></script>
    <script src="./js/lightbox/js/lightbox.min.js"></script>
    <!--<script src="js/mixitup/jquery.mixitup.min.js"></script>-->
    <script src="js/masonry/masonry.pkgd.min.js"></script>
    <script src="js/isotope.pkgd.min.js"></script>

    <script src="js/custom.js?v=3"></script>
    <script src="js/general.js"></script>
    <script src="js/loadPage.js"></script>

    <!-- Add fancyBox main JS and CSS files -->
    <script type="text/javascript" src="js/fancybox/source/jquery.fancybox.js?v=2.1.5"></script>

    <script>
        $(document).ready(function() {
            $(window).scroll(function() {
                if ($(this).scrollTop() > $('body').height() * 0.15) {
                    $('#back-to-top-btn').fadeIn();
                } else {
                    $('#back-to-top-btn').fadeOut();
                }
            });

            $('#back-to-top-btn').click(function() {
                $('html, body').animate({
                    scrollTop: 0
                }, 800);
                return false;
            });
        });
        /*$('#mostre').load('include/pages/mostre.php');

            $( document ).ready(function() {
                $('.fancybox').fancybox();
            });

            $(window).load(function() {
                //isotope
                $container = $('#mixitup');

                $container.isotope({
                    itemSelector: '.mix',
                    masonry: {
                        isFitWidth: true,
                        gutter: 10
                    }
                });

                $('#btn-tutto').bind('click', function(event) {
                    $container.isotope({ filter: '*' });
                });

                $('#btn-giovedi').bind('click', function(event) {
                    $container.isotope({ filter: '.giovedi' });
                });

                $('#btn-venerdi').bind('click', function(event) {
                    $container.isotope({ filter: '.venerdi' });
                });

                $('#btn-sabato').bind('click', function(event) {
                    $container.isotope({ filter: '.sabato' });
                });
                $('#btn-domenica').bind('click', function(event) {
                    $container.isotope({ filter: '.domenica' });
                    //$container.isotope('reloadItems');
                });

                $container.isotope('reloadItems');
                $container.isotope({ filter: '*' });
            });


            //collegamento ai bottoni delle pagine
            $('#concorso-btn').bind('click', function(event) {
                $('#inner-concorso').load('include/concorsoFoto.php');
            });

            $('#form2').submit(function(event){
                var data = $(this).serialize();
                $.post('php/elabora_login.php', data)
                    .success(function(result){
                    $('#risultato-form').html(result);
                })
                    .error(function(){
                    console.log('Error loading page');
                })
                return false;
            });

            //variabili globali
            timeout = 0;
            pagina = '';
            utente = '';
            potere = 0;
            avatar = 'avatar_base.png';
            */
    </script>

</body>

</html>

<?php

?>